/**
 * Created by instancetype on 7/30/14.
 */
'use strict'

const
  async = require('async')
, file = require('file')
, rdfParser = require('./lib/rdf-parser')

, work = async.queue(function(path, done) {
    rdfParser(path, function(err, doc) {
      console.log(doc)
      done()
    })
  }, 1000)

console.log('Beginning to walk the directory...')

file.walk(__dirname + '/cache', function(err, dirPath, dirs, files) {
  files.forEach(function(path) {
    work.push(path)
  })
})